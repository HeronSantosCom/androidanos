<?php
    if (!class_exists("phptail", false)) {
        include path::plugins("phptail/phptail.php");
    }
    new phptail(path::logs() . path::concat(date("Ymd") . ".log"));
?>